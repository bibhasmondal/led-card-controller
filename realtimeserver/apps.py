from django.apps import AppConfig


class RealtimeserverConfig(AppConfig):
    name = 'realtimeserver'
