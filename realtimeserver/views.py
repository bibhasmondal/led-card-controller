import json
from .handler import handler
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


# Create your views here.

def connect(request):
    '''
        Let index.html is a led card controler.
        Connect index.html's websocket client to this websocket server
    '''
    return render(request,'realtimeserver/index.html')

def send(request):
    '''
        Send data to card that's mean to the index.html
    '''
    cardId = '123'
    commandId = handler.send(cardId,{'type': 'loadUrl','url': 'http://google.com'})
    response = handler.recv(commandId)
    return JsonResponse(response)

@csrf_exempt
def command(request,cardId):
    if request.method == 'POST':
        commandId = handler.send(cardId,json.loads(request.POST))
        response = handler.recv(commandId)
        return JsonResponse(response)
    else:
        return JsonResponse({'message':'Invalid request method'})

@csrf_exempt
def loadUrl(request):
    request.body = json.loads(request.POST)
    cardId = request.body.pop('cardId',None)
    request.body["type"] = "loadUrl"
    request.body = json.dumps(request.POST)
    if cardId:
        return command(request,cardId)
    else:
        return JsonResponse({'message':'Card id required'})

@csrf_exempt
def setScreenOpen(request):
    request.body = json.loads(request.POST)
    cardId = request.body.pop('cardId',None)
    request.body["type"] = "callCardService"
    request.body["fn"] = "setScreenOpen"
    request.body = json.dumps(request.POST)
    if cardId:
        return command(request,cardId)
    else:
        return JsonResponse({'message':'Card id required'})

