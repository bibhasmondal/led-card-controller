import uuid
import json
import time
import threading

class Handler:
    def __init__(self,timeout):
        self.channels = {}
        self.responses = {}
        self.timeout = timeout
    
    def addChannel(self,cardId,channel):
        self.channels[cardId] = channel

    def popChannel(self,cardId):
        return self.channels.pop(cardId,None)
    
    def addResponse(self,commandId,res):
        if commandId in self.responses:
            self.responses[commandId] = res

    def popResponse(self,commandId):
        return self.responses.pop(commandId,None)

    def onMessage(self,message):
        try:
            data = json.loads(message.content['text'])
        except Exception as e:
            print(e)
            return
        # Get card id
        cardId = data.get('cardId')
        if cardId:
            channel = self.popChannel(cardId)
            if channel: channel.send({'close':True})
            self.addChannel(cardId,message.reply_channel)
            print('%s connected'%cardId)

        elif data['_type'] == 'restart':
            print('%s restart'%cardId)
            restartReq(cardId)

        elif data['_type'] == 'error':
            self.addResponse(data['_id'],data)

        elif data['_type'] == 'success':
            self.addResponse(data['_id'],data)
        
        else:
            self.addResponse(data['_id'],data)

    def send(self,cardId,data):
        # generate commandId
        commandId = data.get('_id')
        if not commandId:
            commandId = uuid.uuid4().hex
            data['_id'] = commandId
        else:
            commandId = data['_id']
        # get card channel
        channel = self.channels.get(cardId)
        # send data
        if channel:
            channel.send({'text':json.dumps(data)},immediately=True)
        else:
            print("%s does not exist!"%cardId)
        # Add commandId to response
        self.responses[commandId] = None
        # Remove commandId from response
        threading.Timer(interval=self.timeout,function=self.popResponse,args=(commandId,))
        return commandId

    def recv(self,commandId):
        while commandId in self.responses:
            res = self.responses.get(commandId)
            if res:
                return res

# Default global instance of the Handler
handler = Handler(20)
