from . import views
from django.conf.urls import url

app_name = 'realtimeserver'

urlpatterns = [
    url(r'^connect/$', views.connect, name='connect'),
    url(r'^send/$', views.send , name='send'),
	url(r'^command/(?P<cardId>.+)$', views.command, name='command'),
    url(r'^screen/$', views.setScreenOpen, name='screen'),
]
