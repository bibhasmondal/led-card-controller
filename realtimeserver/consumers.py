# In consumers.py
import json
import socket
from .handler import handler
from channels.asgi import get_channel_layer
from channels.sessions import channel_session

def restartReq(cardId):
    return

# Connected to websocket.connect
@channel_session
def ws_connect(message):
    message.reply_channel.send({'accept': True})

# Connected to websocket.receive
@channel_session
def ws_message(message):
    handler.onMessage(message)

# Connected to websocket.disconnect
@channel_session
def ws_disconnect(message):
    message.reply_channel.send({'close':True})
