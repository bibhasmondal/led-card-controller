from . import views
from django.conf.urls import url

app_name = 'rest'

urlpatterns = [
    url(r'^$', views.index, name='home'),
]
